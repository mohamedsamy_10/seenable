<?php

namespace Mosamy\Seenable\Models;

use Illuminate\Database\Eloquent\Model;

class Seenable extends Model
{
    protected $guarded = [];

    public function seenable(){
      return $this->morphTo(__FUNCTION__, 'seenable_type', 'seenable_id');
    }

    public function seener(){
      return $this->morphTo(__FUNCTION__, 'seener_type', 'seener_id');
    }
}
