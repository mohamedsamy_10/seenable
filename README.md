# Laravel Seenable
Monitor to show if a record is seen and by whom

## Installation

```php
composer require mosamy/seenable
```

```php
php artisan migrate
```

## Usage

```php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
  use \Mosamy\Seenable\Seenable;
}
```

> To detect if a message was seen just check **is_seen** property


```php
$message = Message::find(1);
echo $message->is_seen; //boolean
```

#### markAsSeen()
> To mark a record as seen.

```php
$message = Message::find(1);
$message->markAsSeen();
```
#### markAsUnseen()
> To mark a record as unseen.

```php
$message = Message::find(1);
$message->markAsUnseen();
```
#### views

> Get a list of views by users that saw a record.
> If you plan to use this feature in a list don't forget to eager load this function by using **with('views')**
>

```php
$message = Message::find(1);
$views = $message->views;

foreach($views as $view){
	echo $views->seener;
}
```

#### seenBy()

> Get records that have been seen by specific user type **(Model class name)**.

```php
$message = Message::seenBy(App\Models\Admin::class)->get();

// specifiy a user id

$message = Message::seenBy(App\Models\Admin::class, 1)->get();

```

#### seenByMe()

> Get records that have been seen by logged in user.

```php
$message = Message::seenByMe()->get();
```

#### haventSeenBy()

> Get records that haven't been seen by specific user type **(Model class name)**.

```php
$message = Message::haventSeenBy(App\Models\Admin::class)->get();

// specifiy a user id

$message = Message::haventSeenBy(App\Models\Admin::class, 1)->get();

```

#### haventSeenByMe()

> Get records that haven't been seen by logged in user.

```php
$message = Message::haventSeenByMe()->get();
```
#### orderBySeen()

> Order records by seen first.


```php
$message = Message::orderBySeen()->get();
```

#### orderByUnseen()

> Order records by unseen first.

```php
$message = Message::orderByUnseen()->get();
```
