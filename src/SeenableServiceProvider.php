<?php

namespace Mosamy\Seenable;

use Illuminate\Support\ServiceProvider;

class SeenableServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
      $this->loadMigrationsFrom(__DIR__.'/../database/migrations/');
    }
}
