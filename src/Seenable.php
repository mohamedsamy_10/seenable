<?php

namespace Mosamy\Seenable;

use Illuminate\Database\Eloquent\Builder;

trait Seenable
{

  public static function bootSeenable(){
    static::deleting(function ($model) {
      $model->views()->delete();
    });

    static::addGlobalScope('is_seen', function (Builder $builder) {
      $builder->withExists('seen as is_seen');
    });

  }

  public function initializeSeenable(){
    //array_push($this->with, 'seen');
  }

  public function views(){
    return $this->morphMany(\Mosamy\Seenable\Models\Seenable::class, 'seenable');
  }

  public function seen(){
    return $this->morphOne(\Mosamy\Seenable\Models\Seenable::class, 'seenable');
  }

  /*public function scopeWithSeen($query, $seener, $id = null){
    $query->with(['seen' => function($query) use($seener, $id){
      $query->whereHasMorph('seener', [$seener], function($query) use($id){
        $query->when($id, fn($q) => $q->whereId($id));
      });
    }]);
  }

  public function scopeWithSeenByMe($query){
    $query->withSeen(auth()->user()->getTable(), auth()->id());
  }*/

  public function scopeSeenBy($query, $seener, $id = null){
    $query->whereHas('seen', function($query) use($seener, $id){
      $query->whereHasMorph('seener', [$seener], function($query) use($id){
        $query->when($id, fn($q) => $q->whereId($id));
      });
    });
  }

  public function scopeSeenByMe($query){
    $query->SeenBy(get_class(auth()->user()), auth()->id());
  }

  public function scopeHaventSeenBy($query, $seener, $id = null){
    $query->whereDoesntHave('seen', function($query) use($seener, $id){
      $query->whereHasMorph('seener', [$seener], function($query) use($id){
        $query->when($id, fn($q) => $q->whereId($id));
      });
    });
  }

  public function scopeHaventSeenByMe($query){
    $query->HaventSeenBy(auth()->user()->getTable(), auth()->id());
  }

  public function scopeOrderByUnseen($query){
    $query->withCount('seen')->orderBy('seen_count');
  }

  public function scopeOrderBySeen($query){
    $query->withCount('seen')->orderByDesc('seen_count');
  }

  public function markAsSeen(){
    if(!$this->seen){
      $this->seen()->make()
      ->seener()->associate(auth()->user())
      ->save();
    }
  }

  public function markAsUnseen(){
    $this->seen()->delete();
  }

}


?>
